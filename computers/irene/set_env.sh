module purge
# Loading python loads also the correct intel compiler and MPI lib
module load python3/3.7.2
module load lapack
module load plumed

# f90wrap has to be installed and the bin dir that contains it has to be added to the PATH env variable
export PATH=$PATH:$HOME/.local/bin

# This imposes to issue the source command from mw2 directory: 
# cd path/to/mw2
# source computer/irene/set_env.sh
export PYTHONPATH=$PYTHONPATH:$PWD/build/python

