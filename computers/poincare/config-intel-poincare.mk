# Compilation options
F90_PREFIX := 
F90 := $(F90_PREFIX) mpif90
F90STDFLAGS := -g
F90OPTFLAGS := -O2 -xAVX -align array64byte 
F90FLAGS := $(F90STDFLAGS) $(F90OPTFLAGS)
FPPFLAGS := -fpp
LDFLAGS := -llapack
J := -module 
# Path to pFUnit (Unit testing Framework)
PFUNIT :=/gpfshome/mds/staff/amarin/opt/pfunit-parallel-ifort15-ompi163
