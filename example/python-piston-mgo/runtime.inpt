# System configuration
# ====================

# Global simulation parameters
# ----------------------------
num_steps       10     # number of steps in the run
timestep        41.341 # timestep (a.u)
temperature     700    # temperature (K)

# Periodic boundary conditions
# ----------------------------
num_pbc  2

# Thermostat
# ----------
thermostat
   chain_length    5
   relaxation_time 4134.1
   max_iteration   50
   tolerance       1.0e-17

# Species definition
# ---------------
species

  species_type
    name      Na          # name of the species
    count 513             # number of species
    charge point 1.0      # permanent charge on the ions
    mass 22.9897          # mass in amu                    

  species_type
    name       N          # name of the species
    count     513         # number of species
    charge point  0.02    # permanent charge on the ions
    mass   14.007         # mass in amu                 

  species_type
    name       O1         # name of the species          
    count     513         # number of species
    charge point  -0.51   # permanent charge on the ions
    mass   15.999         # mass in amu                 

  species_type
    name       O2         # name of the species          
    count     513         # number of species
    charge point  -0.51   # permanent charge on the ions
    mass   15.999         # mass in amu                 

  species_type
    name       MG1                    # name of the species          
    count     1024                    # number of species
    charge gaussian 0.955234657  2.0  # permanent charge on the ions
    mass   24.305                     # mass in amu                 
    mobile False

  species_type
    name       OW1                    # name of the species         
    count     1024                    # number of species
    charge gaussian 0.955234657  -2.0 # permanent charge on the ions
    mass   15.999                     # mass in amu                 
    mobile False

  species_type
    name       MG2                    # name of the species          
    count     1024                    # number of species
    charge gaussian 0.955234657   2.0 # permanent charge on the ions
    mass   24.305                     # mass in amu                 
    mobile False

  species_type
    name       OW2                    # name of the species         
    count     1024                    # number of species
    charge gaussian 0.955234657  -2.0 # permanent charge on the ions
    mass   15.999                     # mass in amu                 
    mobile False


# Molecule definitions
# --------------------
molecules

  molecule_type
    name sodium   # name of molecule
    count 513     # number of molecules
    sites Na      # molecule's sites

  molecule_type
    name nitrite  # name of molecule  
    count 513     # number of molecules
    sites N O1 O2 # molecule's sites

    # Rigid constraints
    constraint N O1 2.286568
    constraint N O2 2.286568
    constraint O1 O2 4.070470

    constraints_algorithm rattle 1.0e-7 100


# Electrode species definition
# ----------------------------
electrodes

  electrode_type
    name      MG1                      # name of electrode
    species   MG1                      # name of the species
    piston 3.443861840114266e-09 +1.0  # constant pressure 1 atm

  electrode_type
    name      OW1                      # name of electrode         
    species   OW1                      # name of the species
    piston 3.443861840114266e-09 +1.0  # constant pressure 1 atm  

  electrode_type
    name      MG2                      # name of electrode
    species   MG2                      # name of the species
    piston 3.443861840114266e-09 -1.0  # constant pressure 1 atm   

  electrode_type
    name      OW2                      # name of electrode
    species   OW2                      # name of the species
    piston 3.443861840114266e-09 -1.0  # constant pressure 1 atm  

  electrode_charges constant_charge
  compute_force true

# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol       1.6300594548881187e-5  # coulomb rtol
    coulomb_rcut       24.1506                # coulomb cutoff (bohr)
    coulomb_ktol       1e-6                   # coulomb ktol

  fumi-tosi
     ft_rcut 24.1506                          # fumi-tosi cutoff (bohr)
     # fumi-tosi parameters
     ft_pair     Na     Na      1.669540      15.566561         1.7553412       0.0      1.0d10       1.0d10 
     ft_pair     N      N       2.000292      67.034860         18.802272       0.0      1.0d10       1.0d10 
     ft_pair     O1     O1      2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10 
     ft_pair     O2     O2      2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10 
     ft_pair     MG1    MG1     0.971574      262.33379         0.1077253       0.0      1.0d10       1.0d10 
     ft_pair     OW1    OW1     2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10 
     ft_pair     MG2    MG2     0.971574      262.33379         0.1077253       0.0      1.0d10       1.0d10 
     ft_pair     OW2    OW2     2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10 
     ft_pair     Na     N       1.834936      32.303285         5.7449463       0.0      1.0d10       1.0d10 
     ft_pair     Na     O1      1.940724      43.896717         5.7475952       0.0      1.0d10       1.0d10 
     ft_pair     Na     O2      1.940724      43.896717         5.7475952       0.0      1.0d10       1.0d10 
     ft_pair     Na     MG1     1.320566      63.903325         0.4348493       0.0      1.0d10       1.0d10 
     ft_pair     Na     OW1     1.940724      43.896717         5.7475952       0.0      1.0d10       1.0d10 
     ft_pair     Na     MG2     1.320566      63.903325         0.4348493       0.0      1.0d10       1.0d10 
     ft_pair     Na     OW2     1.940724      43.896717         5.7475952       0.0      1.0d10       1.0d10
     ft_pair     N      O1      2.106094      91.093219         18.810943       0.0      1.0d10       1.0d10
     ft_pair     N      O2      2.106094      91.093219         18.810943       0.0      1.0d10       1.0d10
     ft_pair     N      MG1     1.485952      132.61036         1.4231917       0.0      1.0d10       1.0d10
     ft_pair     N      OW1     2.106094      91.093219         18.810943       0.0      1.0d10       1.0d10
     ft_pair     N      MG2     1.485952      132.61036         1.4231917       0.0      1.0d10       1.0d10
     ft_pair     N      OW2     2.106094      91.093219         18.810943       0.0      1.0d10       1.0d10
     ft_pair     O1     O2      2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10
     ft_pair     O1     MG1     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10
     ft_pair     O1     OW1     2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10
     ft_pair     O1     MG2     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10
     ft_pair     O1     OW2     2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10
     ft_pair     O2     MG1     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10
     ft_pair     O2     OW1     2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10
     ft_pair     O2     MG2     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10
     ft_pair     O2     OW2     2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10
     ft_pair     MG1    OW1     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10
     ft_pair     MG1    MG2     0.971574      262.33379         0.1077253       0.0      1.0d10       1.0d10
     ft_pair     MG1    OW2     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10
     ft_pair     OW1    MG2     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10
     ft_pair     OW1    OW2     2.212002      123.78596         18.819617       0.0      1.0d10       1.0d10
     ft_pair     MG2    OW2     1.591798      180.20333         1.4238484       0.0      1.0d10       1.0d10

# Output section
# --------------
output
  default 0
  lammps 100
  step 10
  elec_forces 10
