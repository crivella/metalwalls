#!/bin/bash

# temps CPU a ajuster au calcul
#PBS -l walltime=48:00:00
# memoire a ajuster au calcul
##PBS -l vmem=1gb
# nb procs a ajuster au calcul, voir OMP_NUM_THREADS plus bas
#PBS -l nodes=1:ppn=44
# a changer, nom du job (qstat)
#PBS -N   tfsi_1V_298
#Request that regular output and terminal output go to the same file
#PBS -j oe
#  Request that your login shell variables be available to the job
#PBS -V

# Load the good modules
module load Libraries/impi/2019 Libraries/mkl/2019 Core/Intel/2019

export OMPI_MCA_btl_base_warn_component_unused=0
#export OMPI_MCA_rmaps_base_oversubscribe=1

# Pour savoir sur quel noeud on est
echo "Job lance sur: $HOSTNAME"
echo "StartDir : $PBS_O_WORKDIR"
#echo "(sinon par defaut ceut ete $HOMEDIR)"

# A changer, Startdir = ou sont les fichiers, par defaut HOMEDIR
StartDir=$PBS_O_WORKDIR

# Ne pas modifier, SCRATCHDIR = espace temporaire (local au noeud et a vider apres le calcul)
#SCRATCHDIR=/scratch/$USER/$PBS_JOBID

# Copy everything from submission directory here
SCRATCHDIR=/scratch/$USER/$PBS_JOBID
/bin/mkdir $SCRATCHDIR
/bin/cp -rf $StartDir/* $SCRATCHDIR
cd $SCRATCHDIR

ulimit -s unlimited
date
mpirun /home/bacon/mw_binary/mw_master_27-05-20
date

/bin/cp -r $SCRATCHDIR/* $StartDir
/bin/rm -rf $SCRATCHDIR
exit

