"""Main program to run regression tests for MW
"""
import os.path
from runner import RunnerMW
from validator import ValidatorReferenceFile, ValidatorNISTEnergy
from testrunner import TestRunner

# Create the generic runner to execute MW
mw_root_dir = "../../" # relative path to MW root directory
mw_exe = "mw"          # name of the executable to run
mw_path = os.path.join(mw_root_dir, mw_exe)
mwrunner = RunnerMW(os.path.abspath(mw_path))

# Create generic validator for trajectories output file
trajectoryValidator = ValidatorReferenceFile("trajectories.ref", "trajectories.out", rtol=1e-10,atol=1e-12)

#supercapTestDir = os.path.join(mw_root_dir, "tests/supercap")
#supercapTest = TestRunner(mwrunner, (trajectoryValidator,), supercapTestDir)
#supercapTest.test()

NIST_TestDir = os.path.join(mw_root_dir, "tests/nist")
NIST_SPCE_TestDir = os.path.join(NIST_TestDir, "SPCE-water-reference-calculations")

NIST_SPCE_conf1_TestDir = os.path.join(NIST_SPCE_TestDir, "config1")
NIST_SPCE_conf1_Validator = ValidatorNISTEnergy(E_disp=9.95387e+04, E_LRC=-8.23715e+02, E_real=-5.58889e+05, E_fourier=6.27009e+03, E_self=-2.84469e+06, E_intra=2.80999e+06, E_total=-4.88604e+05)
NIST_SPCE_conf1_Test = TestRunner(mwrunner, (NIST_SPCE_conf1_Validator,), NIST_SPCE_conf1_TestDir)
NIST_SPCE_conf1_Test.test()
