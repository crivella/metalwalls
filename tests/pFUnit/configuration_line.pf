@test
subroutine testEmptyLine()
   use pfunit_mod
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   line = ""
   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(0, config_line%num_words)
end subroutine testEmptyLine

@test
subroutine testBlankLine()
   use pfunit_mod
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   line = "                                                                                                    " // &
         "                                                                                                    " // &
         "                                                        "
   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(0, config_line%num_words)
end subroutine testBlankLine

@test
subroutine testCommentLineBang()
   use pfunit_mod
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   line = "! This is a line with only a comment"
   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(0, config_line%num_words)
end subroutine testCommentLineBang

@test
subroutine testCommentLineHash()
   use pfunit_mod
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   line = "# This is a line with only a comment"
   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(0, config_line%num_words)
end subroutine testCommentLineHash

@test
subroutine testStringValue()
   use pfunit_mod
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line, &
         MW_configuration_line_get_word => get_word
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   character(32) :: word, expected
   line = "keyword"

   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(1, config_line%num_words)
   call MW_configuration_line_get_word(config_line,1,word)

   expected = "keyword"
   @assertEqual(expected, word)
end subroutine testStringValue

@test
subroutine testIntegerValue()
   use pfunit_mod
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line, &
         MW_configuration_line_get_word => get_word
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   integer :: word, expected
   line = "123456"

   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(1, config_line%num_words)
   call MW_configuration_line_get_word(config_line,1,word)

   expected = 123456
   @assertEqual(expected, word)
end subroutine testIntegerValue

@test
subroutine testRealValue()
   use pfunit_mod
   use MW_kinds, only: wp
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line, &
         MW_configuration_line_get_word => get_word
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   real(wp) :: word, expected
   real(wp), parameter :: tolerance = 1.0e-15_wp
   line = "1.23456e+7"

   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(1, config_line%num_words)
   call MW_configuration_line_get_word(config_line,1,word)

   expected = 1.23456e+7_wp
   @assertEqual(expected, word, tolerance)
end subroutine testRealValue

@test
subroutine testTrueValue()
   use pfunit_mod
   use MW_kinds, only: wp
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line, &
         MW_configuration_line_get_word => get_word
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   logical :: word, expected
   line = "True"
   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(1, config_line%num_words)
   call MW_configuration_line_get_word(config_line,1,word)

   expected = .true.
   @assertEqual(expected, word)
end subroutine testTrueValue

@test
subroutine testFalseValue()
   use pfunit_mod
   use MW_kinds, only: wp
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line, &
         MW_configuration_line_get_word => get_word
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   logical :: word, expected
   line = "False"
   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(1, config_line%num_words)
   call MW_configuration_line_get_word(config_line,1,word)

   expected = .false.
   @assertEqual(expected, word)
end subroutine testFalseValue


@test
subroutine testManyValues()
   use pfunit_mod
   use MW_kinds, only: wp
   use MW_configuration_line, only: MW_configuration_line_t, &
         MW_configuration_line_parse_line => parse_line, &
         MW_configuration_line_get_word => get_word
   implicit none

   ! Locals
   ! ------
   character(256) :: line
   type(MW_configuration_line_t) :: config_line
   character(32) :: word_char, expected_char
   integer :: word_int, expected_int
   real(wp) :: word_real, expected_real
   logical :: word_log, expected_log
   real(wp), parameter :: tolerance = 1.0e-15_wp
   line = "   keyword 123456 1.23456e+7 true" // &
         "! a line with 4 values and a comment"

   expected_char = "keyword"
   expected_int = 123456
   expected_real = 1.23456e+7_wp
   expected_log = .true.

   call MW_configuration_line_parse_line(config_line, line)
   @assertEqual(4, config_line%num_words)

   call MW_configuration_line_get_word(config_line,1,word_char)
   call MW_configuration_line_get_word(config_line,2,word_int)
   call MW_configuration_line_get_word(config_line,3,word_real)
   call MW_configuration_line_get_word(config_line,4,word_log)

   @assertEqual(expected_char, word_char)
   @assertEqual(expected_int, word_int)
   @assertEqual(expected_real, word_real, tolerance)
   @assertEqual(expected_log, word_log)
end subroutine testManyValues
