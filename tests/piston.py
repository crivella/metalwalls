import unittest
import os.path
import glob
from shutil import copyfile
import numpy as np

import mwrun

class test_piston(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.test_path = "piston"
    self.mw_exec = os.path.abspath(mwrun.glob_mw_exec)
    self.setup = False

  def run_conf(self, confID, nranks):
    path_to_config = os.path.join(self.test_path, confID)
    n = mwrun.mwrun(self.mw_exec, path_to_config)
    self.workdir = n.workdir

    n.run_mw(nranks)
    self.setup = True

    ok, msg = n.compare_datafiles("electrode_forces.out", "electrode_forces.ref", rtol=1.0E-6, atol=0.0)
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies.out", "energies.ref", rtol=1.0E-6, atol=0.0)
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies_breakdown.out", "energies_breakdown.ref", rtol=1.0E-6, atol=0.0)
    self.assertTrue(ok, msg)

  def tearDown(self):
    if self.setup:
      for f in glob.glob(os.path.join(self.workdir, "*.out")):
        os.remove(f)

  def test_conf_LiCl_Al(self):
     self.run_conf("LiCl-Al", 1)

  def test_conf_LiCl_Al_4MPI(self):
     self.run_conf("LiCl-Al", 4)

