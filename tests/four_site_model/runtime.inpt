# System configuration
# ====================

# Global simulation parameters
# ----------------------------
num_steps        10 # number of steps in the run
timestep     41.341 # timestep (a.u)
temperature   298.0 # temperature (K)

# Periodic boundary conditions
# ----------------------------
num_pbc  3

# Thermostat:
# -----------
thermostat
  chain_length  5
  relaxation_time 4134.1
  tolerance  1.0e-17
  max_iteration  100

# Species definition
# ------------------
species

  species_type
    name           O            # name of the species
    count          215          # number of species      
    charge point   0.0          # permanent charge on the ions
    mass           15.9994      # mass in amu

  species_type
    name           H1           # name of the species    
    count          215          # number of species     
    charge point   0.520        # permanent charge on the ions
    mass           1.008        # mass in amu

  species_type
    name           H2           # name of the species
    count          215          # number of species     
    charge point   0.520        # permanent charge on the ions
    mass           1.008        # mass in amu

  species_type
    name           M            # name of the species
    count          215          # number of species      
    charge point   -1.04        # permanent charge on the ions
    mass           0.0          # mass in amu
    fourth_site_atom            # flags the fourth site

# Molecule definitions
# --------------------
molecules

  molecule_type
    name     TIP4P # name of molecule       
    count    215   # number of molecules
    sites O H1 H2 M  # molecule's sites
    fourth_site 0.283458898 # distance OM
    #fourth_site 0.406291117 # distance OM

    # Rigid constraints
    constraint O   H1   1.808846   # constrained radius for a pair of sites (bohr)
    constraint O   H2   1.808846   # constrained radius for a pair of sites (bohr)
    constraint H1  H2   2.860858   # constrained radius for a pair of sites (bohr)

    constraints_algorithm rattle 1.0e-7 100

# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol       2.08321e-05     # coulomb rtol
    coulomb_rcut       16.  # coulomb cutoff (bohr)
    coulomb_ktol       2.38756e-04     # coulomb ktol

  lennard-jones
     lj_rcut 16.          # lj cutoff (bohr)
     # lj parameters: epsilon in kJ/mol, sigma in angstrom
     lj_pair   O       O  0.649     3.154
 
output
  default 0
  forces 1
  energies 1 Total Kinetic Potential Coulomb van_der_Waals Coulomb_lr Coulomb_keq0 Coulomb_sr Coulomb_self Coulomb_molecule
  pressure 1
  stress_tensor 1
  lammps 1
