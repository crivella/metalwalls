module MW_born_repulsion
   use MW_kinds, only: wp
   use MW_localwork, only: MW_localwork_t
   use MW_molecule, only: MW_molecule_t
   use MW_box, only: MW_box_t
   use MW_ion, only: MW_ion_t
   implicit none
   private

   public :: forces
   public :: energy
contains

   ! ================================================================================
   !> Compute the forces due to born repulsion potential
   subroutine forces(num_pbc, localwork, box, molecules, ions, xyz_ions, force)
      implicit none
      ! Parameters
      ! ----------
      integer, intent(in) :: num_pbc !< number of periodic boundary conditions
      type(MW_localwork_t), intent(in) :: localwork
      type(MW_box_t), intent(in) :: box
      type(MW_molecule_t), intent(in) :: molecules(:)
      type(MW_ion_t), intent(in) :: ions(:)
      real(wp), intent(in) :: xyz_ions(:,:)
      real(wp), intent(inout) :: force(:,:)

      select case(num_pbc)
      case(2)
         call forces_2DPBC(localwork, box, molecules, ions, xyz_ions, force)
      case(3)
         call forces_3DPBC(localwork, box, molecules, ions, xyz_ions, force)
      end select
   end subroutine forces

   ! ================================================================================
   !> Compute the forces due to born repulsion potential
   subroutine energy(num_pbc, localwork, box, molecules, ions, xyz_ions, h)
      implicit none
      ! Parameters
      ! ----------
      integer, intent(in) :: num_pbc !< number of periodic boundary conditions
      type(MW_localwork_t), intent(in) :: localwork
      type(MW_box_t), intent(in) :: box
      type(MW_molecule_t), intent(in) :: molecules(:)
      type(MW_ion_t), intent(in) :: ions(:)
      real(wp), intent(in) :: xyz_ions(:,:)
      real(wp), intent(inout) :: h

      select case(num_pbc)
      case(2)
         call energy_2DPBC(localwork, box, molecules, ions, xyz_ions, h)
      case(3)
         call energy_3DPBC(localwork, box, molecules, ions, xyz_ions, h)
      end select
   end subroutine energy

   ! ================================================================================
   include 'born_repulsion_2DPBC.inc'
   include 'born_repulsion_3DPBC.inc'
   ! ================================================================================
   include 'minimum_image_displacement_2DPBC.inc'
   include 'minimum_image_displacement_3DPBC.inc'
   include 'minimum_image_distance_2DPBC.inc'
   include 'minimum_image_distance_3DPBC.inc'
   ! ================================================================================
end module MW_born_repulsion
