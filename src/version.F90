module MW_version

   implicit none
   private

   public :: MW_version_print

contains

   !===============================================================================
   ! Print code version number and compilation info to ounit
   subroutine MW_version_print(ounit)
#ifndef MW_SERIAL
      use MPI
#endif
      implicit none

      integer, intent(in) :: ounit                ! output unit

      integer :: comp_major, comp_minor, comp_patch

#include "version_git_info.inc"

      ! Compiler information
#if defined(__GFORTRAN__)
      comp_major    = __GNUC__
      comp_minor    = __GNUC_MINOR__
      comp_patch    = __GNUC_PATCHLEVEL__
      write(ounit, '("Compiled with GNU Fortran ",i1,".",i1,".",i1," on ",a12," at ",a10)') &
            __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__,                                       &
            __DATE__, __TIME__

#elif defined(__INTEL_COMPILER)
      comp_major    = __INTEL_COMPILER / 100
      comp_minor    = (__INTEL_COMPILER - 100 * comp_major) / 10
      comp_patch    = __INTEL_COMPILER - 100 * comp_major - 10 * comp_minor
      write(ounit, '("Compiled with Intel Fortran ",i2,".",i2,".",i2," on ",a12," at ",a10)') &
            comp_major, comp_minor, comp_patch,                                                     &
            __DATE__, __TIME__
#elif defined(__PGI)
      write(ounit, '("Compiled with PGI Fortran compiler on ",a12," at ",a10)') &
            __DATE__, __TIME__
#else
      write(ounit, '("Compiled with ?????? on ",a12," at ",a10)') &
            __DATE__, __TIME__
#endif
#ifndef MW_SERIAL
      write(ounit, '("MPI library version ",i1,".",i1)') MPI_VERSION, MPI_SUBVERSION
#endif

#include "version_compilation_flags.inc"
#include "version_module_list.inc"

   end subroutine MW_version_print

end module MW_version
