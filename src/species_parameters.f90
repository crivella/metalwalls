module MW_species_parameters
   use MW_kinds, only: wp, PARTICLE_NAME_LEN
   private

   integer, parameter, public :: SPECIES_NAME_LENGTH = PARTICLE_NAME_LEN !< maximum name length for a species type
   ! Species charge type
   ! -------------------
   integer, parameter, public :: CHARGE_TYPE_NEUTRAL = 0   !< neutral species
   integer, parameter, public :: CHARGE_TYPE_POINT = 1     !< species with a point-charge distribution
   integer, parameter, public :: CHARGE_TYPE_GAUSSIAN = 2  !< species with a gaussian-charge distribution
end module MW_species_parameters


