! Define standard output unit and standard error unit for the whole code
module MW_stdio
   use, intrinsic :: iso_fortran_env, only: output_unit, error_unit
   implicit none
   private

   ! Public subroutines
   ! ------------------
   public :: set_stdout
   public :: set_stderr

   ! Standard output unit
   integer, public, save :: MW_stdout = output_unit

   ! Standard error  unit
   integer, public, save :: MW_stderr = error_unit

contains

   !================================================================================
   ! Set the standard output unit
   !
   ! Output message will be printed to the file connected to the user specified
   ! file unit (ounit).
   ! By default, if the user does not call this routine, output messages are printed
   ! to the standard output_unit.
   subroutine set_stdout(ounit)
      implicit none
      integer, intent(in) :: ounit
      MW_stdout = ounit
   end subroutine set_stdout

   !================================================================================
   ! Set the standard error unit
   !
   ! Error message will be printed to the file connected to the user specified
   ! file unit (eunit).
   ! By default, if the user does not call this routine, error messages are printed
   ! to the standard error_unit.
   subroutine set_stderr(eunit)
      implicit none
      integer, intent(in) :: eunit
      MW_stderr = eunit
   end subroutine set_stderr

end module MW_stdio
